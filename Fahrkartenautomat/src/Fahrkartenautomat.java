import java.util.Scanner;
// kkkkkdjenfjkebfjwebfwfbwekbefkhjwebfkjhbwejkhb
class Fahrkartenautomat {
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args) {

        while (true) {
            int zuZahlenderBetrag = fahrkartenbestellungErfassen();

            int eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);


            fahrkartenAusgeben();

            rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
        }
    }


    //Anzahl und Ticketpreis 
    public static int fahrkartenbestellungErfassen() {

        int zuZahlenderBetrag = 0;
        int anzahlTickets = 0;
        int ticketwahl = 0;
        int rechnung = 0;

        while (ticketwahl != 9) {
            System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n" +
                    "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" +
                    "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" +
                    "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n" +
                    "  Bezahlen (9)\n" +
                    "  Ihre Wahl:");
            ticketwahl = tastatur.nextInt();
            switch (ticketwahl) {
                case 1:
                    zuZahlenderBetrag = 290;
                    break;
                case 2:
                    zuZahlenderBetrag = 860;
                    break;
                case 3:
                    zuZahlenderBetrag = 2350;
                    break;
                case 9:
                    break;
                default:
                    System.out.println("Ungültige Wahl. Bitte erneut wählen");
            }
            if (ticketwahl > 0 && ticketwahl < 4) {
                System.out.print("Anzahl dieser Ticketart: ");
                anzahlTickets = tastatur.nextInt();
                while (anzahlTickets > 10 || anzahlTickets < 1) {
                    System.out.println("Ungueltige Ticketanzahl. Es muss eine Anzahl zwischen 1 und 10 gewählt werden.");
                    System.out.print("Anzahl dieser Ticketart: ");
                    anzahlTickets = tastatur.nextInt();
                }
                zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
                rechnung = rechnung + zuZahlenderBetrag;
                System.out.println("Bisher zu zahlen " + rechnung / 100 + "€");
            }
        }
        return rechnung;
    }

    public static int fahrkartenBezahlen(int rechnung) {
        int eingezahlterGesamtbetrag = 0;
        int eingeworfeneMünze;

        while (eingezahlterGesamtbetrag < rechnung) {
            System.out.printf("Noch zu zahlen: %.2f\n", (((double) (rechnung - eingezahlterGesamtbetrag)) / 100));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = (int) (tastatur.nextDouble() * 100);
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    public static void warte(int timer) {
        try {
            Thread.sleep(timer);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void rueckgeldAusgeben(int rechnung, int eingezahlterGesamtbetrag) {

        int rückgabebetrag = eingezahlterGesamtbetrag - rechnung;
        if (rückgabebetrag > 0) {

            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", (((double) rückgabebetrag) / 100));
            System.out.println("wird in folgenden Münzen ausgezahlt:");


            while (rückgabebetrag >= 200) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 200;
            }
            while (rückgabebetrag >= 100) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 100;
            }
            while (rückgabebetrag >= 50) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 50;
            }
            while (rückgabebetrag >= 20) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 20;
            }
            while (rückgabebetrag >= 10) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 10;
            }
            while (rückgabebetrag >= 5)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 5;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

}
